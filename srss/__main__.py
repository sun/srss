import argparse
import http.server
import socketserver
import srss.sharewood
import srss.rss


class Rss(http.server.BaseHTTPRequestHandler):

    def do_GET(self):
        sw = self.server.sw
        if "req" in self.requestline:
            items = sw.list_req()
            prss = srss.rss.rssize_req(items)
        else:
            items = sw.list_torrents(self.server.url)
            prss = srss.rss.rssize(items)
        self.send_response(200)
        self.send_header('Content-type', 'application/xml')
        self.end_headers()
        self.wfile.write(bytes(prss, "utf-8"))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("user", help="username")
    parser.add_argument("password", help="password")
    parser.add_argument("-p", "--port", type=int, default=8000,
                        help="port on which the server listens")
    parser.add_argument("-u", "--url", help="url to query")
    args = parser.parse_args()

    socketserver.TCPServer.allow_reuse_address = 1
    httpd = socketserver.TCPServer(("", args.port), Rss)
    httpd.sw = srss.sharewood.SW(args.user, args.password)
    httpd.url = args.url

    try:
        print("Serving RSS on port {}".format(args.port))
        httpd.serve_forever()
    except Exception:
        httpd.server_close()
        raise
