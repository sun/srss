import gc
from collections import namedtuple
import re
import robobrowser


BASE = "https://www.sharewood.tv"
TORRENT = BASE + "/torrents"
LAST_TORRENT = '/filterTorrents?_token={}&search=&description=&uploader=&sorting=created_at&direction=desc&qty=25'
REQ = BASE + "/requests"
LAST_REQ = "/filterRequests?_token={}&search=&sorting=created_at&direction=desc&qty=25"
MAXTIME = 60
Torrent = namedtuple('Torrent', ['typet', 'nom', 'href', 'taille'])
Req = namedtuple('Req', ['typet', 'res', 'title', 'href', 'user', 'stat'])


def retry(func):
    def wrapped(self, *args, **kwargs):
        res = func(self, *args, **kwargs)
        if not res:
            print("Relogin")
            self.login()
            res = func(self, *args, **kwargs)
        return res
    return wrapped


class SW(robobrowser.RoboBrowser):
    def __init__(self, user, pass_):
        super().__init__(history=False, parser='html5lib')
        self._user = user
        self._pass = pass_

    def login(self):
        self.open(BASE)
        form = self.get_form(action='https://www.sharewood.tv/login')
        if form:
            form['username'].value = self._user
            form['password'].value = self._pass
            self.submit_form(form)

    @retry
    def list_torrents(self, url):
        torrents = []
        if not url:
            url = LAST_TORRENT
        self.open(TORRENT, stream=False, headers={'Cache-Control': 'no-cache'})
        token = self.find("meta", {'name': 'csrf-token'}).get('content')
        self.open(
            BASE + url.format(token),
            stream=False,
            headers={
                'Cache-Control': 'no-cache'})
        for line in self.find_all(class_="row table-responsive-line"):
            typet = line.find(class_="type-table").img['data-original-title']
            tt = line.find(class_="titre-table")
            badges = [
                e.text for e in tt.find_all(
                    class_="badge-extra badge-res")]
            nom = tt.a.text.strip()
            href = tt.a['href']
            age, taille, comm, seed, leech, compl = [
                e.text.strip()
                for e in line.find_all(class_=re.compile(r'^col-xs-'))]
            typet = re.sub(" torrent", "", typet, count=1)
            if badges:
                nom = " | ".join([nom, ", ".join(badges)])
            torrents.append(Torrent(typet, nom, href, taille))
        gc.collect()
        return torrents

    @retry
    def list_req(self):
        req = []
        self.open(REQ)
        token = self.find("meta", {'name': 'csrf-token'}).get('content')
        self.open(BASE + LAST_REQ.format(token))
        body = self.find("tbody")
        if not body:
            return req
        for line in body.find_all("tr"):
            fields = []
            for col in line.find_all("td"):
                if not fields:
                    f = col.find(class_=re.compile(r'^fal'))
                    fields.append(f['data-original-title'].strip())
                else:
                    fields.append(col.text.strip())
                a = col.find('a', class_="view-torrent")
                if a:
                    fields.append(a['href'])
            typet, res, title, href, user, vote, comm, rec, age, stat = fields
            typet = re.sub(" Demande", "", typet, count=1)
            req.append(Req(typet, res, title, href, user, stat))
        return req
