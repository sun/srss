import html

HEAD = """<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0">
<channel>
<title>Sharewood RSS</title>
<link>https://www.sharewood.tv</link>
"""
FOOT = """
</channel>
</rss>"""


def xml(func):
    def wrapped(items):
        return HEAD + func(items) + FOOT
    return wrapped


@xml
def rssize(torrents):
    rss_tpl = "<description>Sharewood unofficial rss for last torrents</description> {items}"
    item_tpl = """<item><title>{torrent.typet} | {nom} | {torrent.taille}</title>
            <link>{torrent.href}</link>
            </item>"""
    return rss_tpl.format(items="\n".join(
        item_tpl.format(torrent=torrent, nom=html.escape(torrent.nom))
        for torrent in torrents))


@xml
def rssize_req(reqs):
    rss_tpl = "<description>Sharewood unofficial rss for last req</description> {items}"
    item_tpl = """<item><title>{req.typet} | {title} | {req.res} | {req.user} | {req.stat}</title>
            <link>{req.href}</link>
            </item>"""
    return rss_tpl.format(items="\n".join(
        item_tpl.format(req=req, title=html.escape(req.title))
        for req in reqs))
