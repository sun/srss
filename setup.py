import pathlib
from setuptools import setup, find_packages

setup(
    name="srss",
    version="0.0.8",
    description="Sharewood rss",
    packages=find_packages(),
    install_requires=["robobrowser", "html5lib"],
    entry_points={'console_scripts': ['srss=srss.__main__:main']},
)
